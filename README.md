DPASTE(1) - General Commands Manual

# NAME

**dpaste** - simple wrapper for dpaste.de

# SYNOPSIS

**dpaste**
\[file
\[lexer
\[expire]]]

# DESCRIPTION

The
**dpaste**
utility is a simple wrapper for the dpaste.de service, which lets user
upload text snippets that expires after a specified amount of
time. The service also offer a web view of the snippet with optional
syntax highlighting.

This utility will invoke
curl(1)
to post the given input to dpaste.de and, if all went well, print to
stdout the link to the resource.

# OPTIONS

file

> The file to read from, or '-' for stdin. The default is '-'

lexer

> The name of the lexer to use. By default is
> `_text`,
> but also programming language, such as
> *python*
> or
> *bash*
> are accepted. For a full list please consult the dpaste.de website.

expires

> How long the snippet will be available; can be
> `single`, `one`, `one-time`, `one_time`
> for a one-shot snippet or
> `hour`, `day`, `week`
> and
> `month`,
> optionally preceded by
> `one`, `one-`
> or
> `one_`. If you supply something different it'll be passed
> directly to dpaste.de.

# CAVEATS

This may sound weird, but
*sh*
or
*zsh*
aren't recognized as languages, while
*bash*
seems to work fine for various
sh(1)
derived scripting languages.

OpenBSD 6.3 - June 23, 2018
